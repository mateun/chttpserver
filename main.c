#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>

int main(int argc, char** args) 
{

  printf("Starting HTTP server at port 5000\n");
  int listenfd = 0, connfd = 0;
  struct sockaddr_in server_address;
  
  char sendBuff[1025];
  time_t ticks = 0;

  listenfd = socket(AF_INET, SOCK_STREAM, 0);
  memset(&server_address, '0', sizeof(server_address));
  memset(sendBuff, '0', sizeof(sendBuff));

  server_address.sin_family = AF_INET;
  server_address.sin_addr.s_addr = htonl(INADDR_ANY);
  server_address.sin_port = htons(5000);

  bind(listenfd, (struct sockaddr*)&server_address, sizeof(server_address));
  listen(listenfd, 10);

  int running = 1;
  while (running) 
  {
    printf("waiting for connection:\n");
    connfd = accept(listenfd, (struct sockaddr*)NULL, NULL);
    ticks = time(NULL);
    printf("received a connection \n");
    int nread = 0;
    char recvBuff[2048];
    int receiving = 1;
      printf("in wait for rcv\n");
      nread = read(connfd, recvBuff, sizeof(recvBuff)-1);
      {
	 printf("recv in bytes: %d\n", nread);
         recvBuff[nread] = 0;
         if(fputs(recvBuff, stdout) == EOF)
         {
           printf("\n Error : Fputs error\n");
         };
      } 

      int written = snprintf(sendBuff, sizeof(sendBuff), "HTTP/1.1 200 OK\r\n");
      written += snprintf(sendBuff+written, written - sizeof(sendBuff), "Connection : close\r\n");
      snprintf(sendBuff + written, written - sizeof(sendBuff), "Date : %.24s\r\n\r\nthis is the body text", ctime(&ticks));
      printf("response: %s\n", sendBuff);
      write(connfd, sendBuff, strlen(sendBuff));
    close(connfd);

  }
  return 0;
}

